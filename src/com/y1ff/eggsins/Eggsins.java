package com.y1ff.eggsins;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/* eggsins - brazzerfy your images!
 *
 * @author desire
 * @since 1/1/2013
 */

public class Eggsins extends JFrame {
	private static Eggsins instance = new Eggsins();
	
	public static void main(String [] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			final JFileChooser fc = new JFileChooser();
			final int showOpenDialog = fc.showOpenDialog(instance);
			if (showOpenDialog != JFileChooser.APPROVE_OPTION) return;
			final File f = fc.getSelectedFile();
			instance.overlayBrazzers(f.getPath());
		}catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	private void overlayBrazzers(String input) throws Exception {
		final File output = new File("eggsins" + new Random().nextInt() + ".png");
		final BufferedImage in = ImageIO.read(new File(input));
		final BufferedImage brazzers = ImageIO.read(new File(Eggsins.class.getResource("/com/y1ff/eggsins/resources/brazzers.png").getFile()));
		final BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);
		out.getGraphics().drawImage(in, 0, 0, null);
		out.getGraphics().drawImage(brazzers, in.getWidth() - brazzers.getWidth(),in.getHeight() - brazzers.getHeight(), null);
		ImageIO.write(out, "PNG", output);
		JOptionPane.showMessageDialog(null, "Outputted to " + output.getPath(), "eggsins", JOptionPane.PLAIN_MESSAGE);
	}
}


